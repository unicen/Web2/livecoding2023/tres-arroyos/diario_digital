<?php
require_once (PATH_SITE.'/lib/helperHtml.php');
require_once (PATH_SITE.'/lib/fakeDDBB.php');

function showHome() {
  showHeader();
  
  $noticiasDB = getNoticiasDB();

  echo ('<main class="container mt-5">');
  echo ('<section class="noticias">');
  
  foreach ($noticiasDB as $indice => $noticia){ 
    echo ('<div class="card">');
    echo ('    <img src="'.$noticia->img.'" class="card-img-top" alt="...">');
    echo ('<div class="card-body">');
    echo ('<h5 class="card-title">'.$noticia->title.'</h5>');
    echo ('<p class="card-text">'.$noticia->text.'</p>');
    echo ('<a class="btn btn-outline-primary" href="noticia.php?id='.$indice.'">Leer más</a>');
    echo ('</div>');
    echo ('   </div>');
  }

  echo ('</section>'); 
  echo ('</main>');

  showFooter();
}
