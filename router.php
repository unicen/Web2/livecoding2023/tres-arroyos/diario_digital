<?php
define ('PATH_SITE', dirname(__FILE__));
define ('BASE_URL', '//'.$_SERVER['SERVER_NAME'] . ':' . $_SERVER['SERVER_PORT'] . dirname($_SERVER['PHP_SELF']).'/');
require_once('contenido/home.php');
require_once('contenido/noticia.php');
require_once('contenido/about.php');

// leemos la accion que viene por parametro
$action = 'home'; // acción por defecto

if (!empty($_GET['action'])) { // si viene definida la reemplazamos
    $action = $_GET['action'];
}

// parsea la accion Ej: dev/juan --> ['dev', juan]
$params = explode('/', $action);

// determina que camino seguir según la acción
switch ($params[0]) {
    case 'home':
        showHome();
        break;
    case 'noticia':
        showNoticia($params[1]);
        break;

        case 'noticias': {
            if (isset($_GET['texto'])) {
                echo ("<p>Buscara noticias que incluyan texto".$_GET['texto']);
            }
            if (isset($_GET['sort'])) {
                echo ("<p>Las mostrará ordenadas por ".$_GET['sort']);
            }

            showNoticia($params[1]);}
            break;

    case 'about':
        showAbout();
        break;
    default:
        echo('404 Page not found');
        break;
}

