<?php
require_once (PATH_SITE.'/lib/helperHtml.php');
require_once (PATH_SITE.'/lib/fakeDDBB.php');

function showAbout()
{
  showHeader();
  $creadoresDB = getCreadores();

  echo ('<main class="container mt-5">');
  echo ('<section class="noticias">');

  foreach ($creadoresDB as $creador) {
    echo ('<div class="card">');
    echo ('<div class="card-body">');
    echo ('<h5 class="card-title">Creador</h5>');
    echo ('<p class="card-text">' . $creador . '</p>');
    echo ('</div>');
    echo ('</div>');
  }
  echo ('</section>');
  echo ('</main>');

  showFooter();
}
