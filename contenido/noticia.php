<?php 
require_once (PATH_SITE.'/lib/helperHtml.php');
require_once (PATH_SITE.'/lib/fakeDDBB.php');

function showNoticia($id) {
    showHeader();
  
    $noticiasDB = getNoticiasDB();    
    echo ('<section class="noticias">');
    echo ('<div class="card">');
    echo ('    <img src="'.$noticiasDB[$id]->img.'" class="card-img-top" alt="...">');
    echo ('    <div class="card-body">');
    echo ('    <h5 class="card-title">'.$noticiasDB[$id]->title.'</h5>');
    echo ('    <p class="card-text">'.$noticiasDB[$id]->text.'</p>    ');
    echo ('    </div>');
    echo ('</div>');
    echo ('</section>');

    showFooter();
}

