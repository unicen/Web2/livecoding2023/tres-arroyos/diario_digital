<?php 
require 'templates/header.html';
require_once 'fakeDDBB.php';
$id = $_GET['id'];
?> 

<section class="noticias">
    <div class="card">
        <img src="<?php echo $noticiasDB[$id]->img?>" class="card-img-top" alt="...">
        <div class="card-body">
        <h5 class="card-title"><?php echo $noticiasDB[$id]->title?></h5>
        <p class="card-text"><?php echo $noticiasDB[$id]->text?></p>    
        </div>
    </div>
</section>
<?php require 'templates/footer.html'; ?>